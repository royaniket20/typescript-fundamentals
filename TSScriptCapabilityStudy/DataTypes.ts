let firstNameData: string;

firstNameData = "Aniket";

let newName = firstNameData.toUpperCase() + "Some Test";

console.log(`This Name Updated to :: ${newName}`);

let age = 25;

age = 25.3;

console.log(`Age is - ${age}`);

age = parseInt("26.33"); //Due to type Inference TS knows what can be type of variable

let decision: Boolean = false;

console.log(decision);

//Array

let empList = []; //Type is of any[]

let NewempList: String[] = new Array();

NewempList.push("Aniket");
NewempList.push("Amit");
NewempList.push("Aloke");
NewempList.push("Ashoke");

let empArray: Array<number> = [1, 3, 4, 5, 6, 7, 8]; //Do not use Number

let filteredRes = empArray.find((item) => item === 3);

console.log(`Result is - ${filteredRes}`);

let filteredArr = empArray.filter((item) => item > 3);

console.log(`Result is - ${filteredArr}`);

let initialValue = 100;
let sumOfNums = empArray.reduce((accu, item) => accu + item, initialValue);

console.log(`Result Of Sum  - ${sumOfNums}`);

//Enum is TypeScript

enum Color {
  Reg,
  Green,
  Blue,
}

let colorSelect: Color = Color.Blue;
console.log(colorSelect);

const enum Food {
  Maggi,
  Biriyani, //Will be removed during compilation
  Pepsi, //Will be removed during compilation
}

let foodSelect: Food = Food.Maggi;
console.log(foodSelect);

//Tuple Example
let dataTuples: [number, number];

function swapNumbers(num1: number, num2: number): [number, number] {
  return [num2, num1];
}

dataTuples = swapNumbers(10, 20);

console.log(`First Number - ${dataTuples[0]}`);
console.log(`First Number - ${dataTuples[1]}`); //You cannot use >1 Index

let anyArr: any[];

anyArr = [];

anyArr.push(23);
anyArr.push("Aniket");
anyArr.push(true);

console.log(anyArr);

function add3(a: number, b: number, ...c: number[]): number {
  return a + b + c.reduce((accum, num) => accum + num, 0);
}

let sumOfArgs = add3(1, 4, 6, 7, 8, 2, 3, 4, 5);
console.log(`Sum of Var args - ${sumOfArgs}`);
let ipNums = [6, 7, 8, 2, 3, 4, 5];
sumOfArgs = add3(1, 4, ...ipNums);
console.log(`Sum of Var args - ${sumOfArgs}`);

//Generics

function genericTypeDemo<Type1, Type2, Type3>(
  param1: Type1,
  param2: Type2,
  param3: Type3
): [Type2, Type3] {
  return [param2, param3];
}

let resOfGenerics: [number, boolean] = genericTypeDemo<string, number, boolean>(
  "Aniket",
  31,
  true
);
console.log(`resOfGenerics : ${resOfGenerics}`);
