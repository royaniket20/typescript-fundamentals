interface Car {
  engine: String;
  bonet: String;
  myAge: Number;
}

let car: Car[] = [
  { engine: "engine", bonet: "bonet", myAge: 100 },
  { engine: "engine", bonet: "bonet", myAge: 200 },
  { engine: "engine", bonet: "bonet", myAge: 300 },
  { engine: "engine", bonet: "bonet", myAge: 400 },
  { engine: "engine", bonet: "bonet", myAge: 500 },
  { engine: "engine", bonet: "bonet", myAge: 600 },
];

let individualCard: Car = { engine: "engine", bonet: "bonet", myAge: 100 };
let { engine: myEngine, myAge }: Car = individualCard;

console.log(`Age picked Up = ${myAge} | ${individualCard.myAge}`);
console.log(`Engine Picked Up - ${myEngine} | ${individualCard.engine}`); //Variable Renamed

let [car1, car2, ...restCars]: Car[] = car;

console.log(
  `Selected Cars - ${JSON.stringify(car1)} | ${JSON.stringify(
    car2
  )} | ${JSON.stringify(restCars)}`
);
