export interface Address {
  name: String;
  street: String;
  pin: String;
  optionalProp?: any;
}

export interface HomeAddress extends Address {
  additionalFiled: String;
  getAddress(): Address;
}

let addr: Address = {
  name: "Home Address",
  street: "NH 6",
  pin: "4455656",
};

console.log(JSON.stringify(addr));

//Now we can use this as Data Type in any Class It needed

class SuperMan {
  address!: Address;
  //We could do like below - But this breaks design principals
  addressBad!: { name: String; street: String; pin: String };
  constructor(
    address: Address,
    addressBad: { name: String; street: String; pin: String }
  ) {
    this.address = address;
  }
}

//sO WE CAN NOW SURELY sAY WHAT tYPE OF dATA BEING PASSED
let superMain: SuperMan = new SuperMan(
  {
    name: "Home Address",
    street: "NH 6",
    pin: "4455656",
  },
  {
    name: "Home Address",
    street: "NH 6",
    pin: "4455656",
  }
);
