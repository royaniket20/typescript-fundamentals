//When  ES5 is used as TSConfig Target
//  * "use strict";
// var Student = /** @class */ (function () {
//   function Student() {
//   }
//   return Student;
// }());
//When ES2016 is used

import { HomeAddress, Address } from "./Human";
class Student implements HomeAddress {
  private id!: number; //This cannot be accessed Directly  - You can Have Public Getter method
  name!: String;
  address!: String;
  //New Approach - #
  #password!: String;
  someFiled!: String;
  protected aspiration!: String;

  constructor(id: number, name: string, address: string) {
    this.address = address;
    this.id = id;
    this.name = name;
    this.#password = "Secret-Password";
  }

  //COMING FROM IMPLEMENTED INTERFACE
  additionalFiled!: String;
  street!: String;
  pin!: String;
  optionalProp?: any;
  getAddress(): Address {
    throw new Error("Method not implemented.");
  }
  //COMING FROM IMPLEMENTED INTERFACE

  logStrudent(msg: String): void {
    console.log(`This is a Log Message : ${msg}`);
  }

  static staticExample(): void {
    console.log(`I am Student Static Method`);
  }

  giveAddress(): String {
    return this.address;
  }
}

class Aniket extends Student {
  job!: String;

  constructor(job: String, aspiration: String) {
    super(100, "Aniket", "Bagnan");
    this.job = job;
    this.aspiration = aspiration;
  }

  static staticExample(): void {
    console.log(`I am Aniket Static Method`);
  }

  //Method is Overridden
  logStrudent(msg: String): void {
    console.log(`This is a Log Message : ${msg} | ${this.aspiration}`);
  }
}

let std: Student = new Student(100, "Aniket", "Bagnan");

console.log(
  `Student is - ${JSON.stringify(std)} | someFiled = ${std.someFiled}`
);
console.log(`This is the address - ${std.giveAddress()}`);
//console.log(std.id , std.#password); //They are Now private Member and cannot be accessed

let aniket: Aniket = new Aniket("Engineer", "VP");
aniket.logStrudent("Hi Aniket");
std.logStrudent("Hi Aniket");

Aniket.staticExample(); //This is a Static Method
Student.staticExample(); //This is a Static Method
